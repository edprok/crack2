#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *hsh;
    // Hash the guess using MD5
    hsh = md5(guess, strlen(guess));
    //printf("%s\n", hsh);
    // Compare the two hashes
   // printf("HASH: %s \n hsh:  %s \n", hash, hsh);
    if (strncmp(hsh, hash, HASH_LEN) == 0)
    {
 
        free(hsh);
        return 1;
    } else 
    {
        free(hsh);
        return 0;
    }
    // Free any malloc'd memory

}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    //get the size
    struct stat info;
    stat(filename, &info);
    int file_len = info.st_size;
    
    //DEBUG
   // printf("Size: %d\n", file_len);
        //alloc some space
    char *chunk = (char *)malloc(file_len+1);
    
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("can't open that file");
        exit(1);
    }
    fread(chunk, 1, file_len, f);
    fclose(f);
    
    chunk[file_len] = '\0';
    
    int lines = 0;
    for (int i = 0; i < file_len; i++)
    {
        if (chunk[i] == '\n')
        {
            chunk[i] = '\0';
            lines++;
        }
    }
    if (chunk[file_len-1] != '\0') lines++;

    char **strings = malloc((lines+1) * sizeof(char *));

    int string_idx = 0;
    for (int i = 0; i < file_len; i += strlen(chunk+i) + 1)
    {
        strings[string_idx] = chunk+i;
        string_idx++;
    }
    

    
    return strings;
    free(strings);
    free(chunk);
    
    

}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int x = 0;
    int y = 0;
    
    while(hashes[x] != NULL)
    {
        while(dict[y] != NULL)
        {
            if ( tryguess(hashes[x], dict[y]) == 1)
            {
            printf("%s : %s \n", hashes[x], dict[y]);
            }
            y++;
        }
        y=0;
        x++;
    }

}
